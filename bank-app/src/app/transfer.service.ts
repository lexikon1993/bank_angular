import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  constructor(private authService: AuthService) { }

  fullRegisterInfo = this.authService.registerInfo;

  increaseAmount(id: number, amount: number) {
    this.fullRegisterInfo.forEach((element: any) => {
      if (element.id === id) {
        element.amount += amount;
        alert('Account amount was successfully increased!');
      }
    });
    console.log('info end: ', this.fullRegisterInfo);
  }

  decreaseAmoung(id: number, amount: number) {
    this.fullRegisterInfo.forEach((element: any) => {
      if (element.id === id) {
        element.amount -= amount;
        alert('Account amount was successfully decreased!');
      }
    });
    console.log('info end: ', this.fullRegisterInfo);
  }
}
