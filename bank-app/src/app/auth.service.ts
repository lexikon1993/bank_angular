import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  registerInfo: any = [{
    id: 1,
    amount: 100,
    firstName: 'Oleksii',
    secondName: 'Iepikhin',
    email: '1lexikon2@gmail.com',
    password: '123456'
  }];

  signInInfo: any = [];

  constructor(private router: Router) {}

  openAccount(firstName: string, secondName: string, email: string, password: string) {
    const amount = 100;
    let id = 1;

    id++;

    this.registerInfo.push({id, amount, firstName, secondName, email, password});

    this.router.navigate(['/transfer']);
    alert('You were successfully registered!');
  }

  signIn(email: string, password: string) {
    let loginSuccess = false;
    this.signInInfo.push({email, password});

    this.registerInfo.forEach((element: any) => {
      if (element.email === this.signInInfo[0].email && element.password === this.signInInfo[0].password) {
        loginSuccess = true;
        this.router.navigate(['/transfer']);
        alert('Your login was successfull');
      }
    });

    if (!loginSuccess) {
      alert('The user was not found!');
    }

    this.signInInfo = [];
  }
}
