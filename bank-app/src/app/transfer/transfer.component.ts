import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TransferService } from '../transfer.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {
  allTypes = ['Increase', 'Decrease'];
  currentAmount: number;

  constructor(private transferService: TransferService) { }

  ngOnInit() {}

  onChangeAmount(form: NgForm) {
    const type = form.value.type;
    const amount = form.value.amount;
    const id = form.value.id;

    if (type === 'Increase') {
      this.transferService.increaseAmount(id, amount);
    } else if (type === 'Decrease') {
      this.transferService.decreaseAmoung(id, amount);
    }
  }

}
