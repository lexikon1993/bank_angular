import { TestBed } from '@angular/core/testing';

import { TransferService } from './transfer.service';
import { Router } from '@angular/router';

class MockRouter {
  navigate = jasmine.createSpy('navigate');
}

describe('TransferService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: Router, useClass: MockRouter
    }]
  }));

  it('should be created', () => {
    const service: TransferService = TestBed.get(TransferService);
    expect(service).toBeTruthy();
  });
});
